import org.openqa.selenium.WebDriver;

public class DriverManager {

    private WebDriver webDriver = null;

    public DriverManager() {
    }

    ;

    public WebDriver getWebDriver() {
        return webDriver;
    }

    public void setWebDriver(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

}
