import org.openqa.selenium.WebDriver;

/**
 * Layout class
 **/
public class Base {

    protected WebDriver driver;

    public Base(WebDriver driver) {
        this.driver = driver;
    }

    public void open(String url) {
        driver.get(url);
    }
}
