import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CategoryPage extends Base {

    private final By h1 = By.tagName("h1");

    public CategoryPage(WebDriver webDriver) {
        super(webDriver);
    }

    public String getH1Text() {
        return driver.findElement(h1).getText();
    }

}
