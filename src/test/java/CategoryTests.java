import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CategoryTests extends BeforeAndAfter {

    private CategoryPage categoryPage;

    private String testCategoryUrl = "https://rozetka.com.ua/mobile-phones/c80003/";
    private final String testNecessaryH1Text = "Мобильные телефоны";

    @BeforeClass
    public void beforeClass() {
        categoryPage = new CategoryPage(getWebDriver());
    }

    @Test
    public void checkH1TextTest() {
        categoryPage.open(testCategoryUrl);

        Assert.assertEquals(categoryPage.getH1Text(), testNecessaryH1Text, "Text of H1 tag equal \"" + testNecessaryH1Text + "\"");
    }

}
