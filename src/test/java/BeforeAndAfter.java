import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import java.util.concurrent.TimeUnit;

public class BeforeAndAfter extends DriverManager {

    @Parameters("browser")
    @BeforeTest
    public void beforeTest(@Optional("chrome") String browser) {
        switch (browser.toLowerCase()) {
            case "chrome":
                System.setProperty("webdriver.chrome.driver", ".\\src\\main\\resources\\chromedriver.exe");
                setWebDriver(new ChromeDriver());
                break;
            case "firefox":
                System.setProperty("webdriver.gecko.driver", ".\\src\\main\\resources\\geckodriver.exe");
                setWebDriver(new FirefoxDriver());
                break;
            case "ie":
                System.setProperty("webdriver.ie.driver", ".\\src\\main\\resources\\IEDriverServer.exe");
                setWebDriver(new InternetExplorerDriver());
                break;
            default:
                System.out.println("Incorrect browser name");
                return;
        }
        getWebDriver().manage().window().maximize();
        getWebDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @AfterTest
    public void afterTest() {
        if (getWebDriver() != null)
            getWebDriver().quit();
    }
}
